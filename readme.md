# edx Video Finder/Scraper

## Overview

Given a web page for a particular course topic on [edx](https://courses.edx.org), 
scrapes it for video links, and downloads them. The webpage needs to basically 
be saved-as from the browser, this is not yet a fully-automated solution that 
automatically logs-in, etc.


## Usage

Clone/download the repository, cd into the checkout directory and run:

        python run.py -h

Example:
        
        # Simulate run with -n, prints video links and where they will be downloaded
        python run.py -n -d ~/coursework-videos/cs110x/week-2/ lecture-1.html lecture-2.html


## Module Dependencies

This module has 2 external dependencies:

* [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/).
* [Requests](https://pypi.python.org/pypi/requests)

No packaging of the module is done, and so you will need to manually install
this. Highly recommend the [Anaconda](https://www.continuum.io/why-anaconda) 
(or miniconda) Python distribution for ease of installation and update. Then
run:

        conda install beautifulsoup4 requests

Feel free to contribute a distutils packaging configuration, too. :-)


## License

Released under [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0).

