import argparse
import os
import pathlib
import sys
from argparse import RawTextHelpFormatter
from typing import Sequence

sys.path.append(os.getcwd())

from edx.parse import Parser
import edx.util as util
from edx.download import Downloader

DESCRIPTION = """
Find and download videos from a course page file on courses.edx.com.

Example, simulation only (-n), shows what will be downloaded:
$ python run.py -n -d ~/coursework-videos/cs110x/week-2/ lecture-2.html

Remove -n to actually do the download.

This is not a fully-automated solution. All it does is, from a given webpage's
source, finds video links, and downloads them. The web page is behind a login,
however, videos are hosted on CDNs, and do not require authentication, allowing
the tool to download them.

Save the web page (HTML only) for a particular section/topic of a week (left
menubar). Each topic will have tabs (top bar on page with icons) for the various
materials: quizzes, notes, videos, etc.

Saving a webpage for a topic will bring in all the video links across all tabs
for that topic. You will need to save multiple pages for each topic of the
week, but not visit individual tabs, which are just links in the same page.

For example, given a course structure like the following:

>> Week 1
Topic 1 > Tab 1, Tab 2, Tab 3
Topic 2 > Tab 1, Tab ... Tab N

>> Week 2
Topic 1 > Tab 1, Tab 2, Tab 3
Topic 2 > Tab 1, Tab ... Tab N
Topic 3 > Tab 1, Tab ... Tab N

To get all videos, you need to visit and save the web pages for:

Week 1: Topic 1 and 2
Week 2: Topic 1, 2, and 3.

A total of 5 web pages.
"""

class Runner:
  class DownloadOperation:
    def __init__(self, downloadUrl: str, numPrefix: int, outputDirectory: str):
      self.outputDirectory = outputDirectory
      p = pathlib.Path(outputDirectory)
      self.downloadUrl = downloadUrl
      self.outputFileName = '%03d-%s' % (numPrefix, util.getBaseFileName(downloadUrl))
      self.outFilePath = p / self.outputFileName

    def apply(self):
      Downloader().download(self.downloadUrl, self.outputDirectory, self.outputFileName)

    def __str__(self):
      return '%s -> %s' % (self.downloadUrl, self.outFilePath)

  def __init__(self, htmlFilePaths: Sequence[str]):
    self.results = [Parser(path).parse() for path in htmlFilePaths]

  def download(self, downloadDir: str, simulate: bool = True):
    for r in self.results:
      print(r.htmlFilePath + ':')
      for i, l in enumerate(r.links):
        op = Runner.DownloadOperation(l, i+1, downloadDir)
        print(op)
        if not simulate:
          op.apply()
      print()

######################

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description=DESCRIPTION, formatter_class=RawTextHelpFormatter)
  parser.add_argument('webpages', metavar='webpage', type=str, nargs='+', help='web pages to find video links to download')
  parser.add_argument('-d', '--download-directory', help='download output directory', required=True)
  parser.add_argument('-n', '--no-download', action='store_true', help='Download simulation only, shows what would be done')

  args = parser.parse_args()

  try:
    runner = Runner(args.webpages)
    simulationOnly = True if args.no_download else False
    runner.download(args.download_directory, simulationOnly)
  except KeyboardInterrupt:
    print("User cancelled.")

