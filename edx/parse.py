import re
from typing import Sequence

from bs4 import BeautifulSoup


class Parser:
  class ParseResult:
    def __init__(self, htmlFilePath: str, links: Sequence[str]):
      self.htmlFilePath = htmlFilePath
      self.links = links

    def __str__(self) -> str:
      return '%s [Found %d video(s)] -> %s' % (
        self.htmlFilePath,
        len(self.links),
        self.links
      )

  def __init__(self, htmlFilePath: str):
    self.htmlFilePath = htmlFilePath

  def parse(self):
    with open(self.htmlFilePath) as f:
      soup = Parser.__parseHtml(f)
      return Parser.ParseResult(
        self.htmlFilePath,
        Parser.__getVideoLinks(soup)
      )

  @staticmethod
  def __getVideoLinks(soup: BeautifulSoup) -> Sequence[str]:
    videoContainerDivs = soup.find_all(
      'div',
      id=lambda v: v.startswith('seq_contents_') if v else False
    )
    links = []

    for div in videoContainerDivs:
      s = Parser.__parseHtml(div.get_text())  # Getting text will unescape the escaped HTML it seems
      anchors = s.find_all('a', text=re.compile('Download video'))
      for a in anchors:
        links.append(a['href'])

    return links

  @staticmethod
  def __parseHtml(data) -> BeautifulSoup:
    """data is a string or file-like object"""
    return BeautifulSoup(data, 'html.parser')
