from urllib.parse import urlparse

import requests


def assertOk(r: requests.Response) -> None:
  assert r.status_code == requests.codes.ok


def getBaseFileName(url: str) -> str:
  return urlparse(url).path.split('/')[-1]
