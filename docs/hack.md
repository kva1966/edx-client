# General Approach

## Access login page to get session cookie:

https://courses.edx.org/login

## Login

url-form-encoded
POST https://courses.edx.org/user_api/v1/account/login_session/

email
password
remember -> true|false

Response: 200 OK, or 403 if bad login

## Access Dashboard Page

https://courses.edx.org/dashboard

Then find individual course pages at in body blobs like so:

      <div class="wrapper-course-details">
        <h3 class="course-title">
              <a data-course-key="course-v1:CaltechX+CS_1156x+3T2016" href="/courses/course-v1:CaltechX+CS_1156x+3T2016/info">Learning From Data (introductory Machine Learning course)</a>
        </h3>

## Access Each Course Page to Find Videos

Replace /info from link found above with /courseware

/courses/course-v1:CaltechX+CS_1156x+3T2016/info
-> /courses/course-v1:CaltechX+CS_1156x+3T2016/courseware

This will respond with a redirect to the last known/remembered location:

302, with a header like so:

Location	https://courses.edx.org/courses/course-v1:BerkeleyX+CS110x+2T2016/courseware/d1f293d0cb53466dbb5c0cd81f55b45b/035f696c1172403f9253736f5dc2dccb/

However, each page will have all the links for each week.

<div class="chapter-content-container" id="week-1-big-data-and-data-science-child" tabindex="-1" role="group" aria-label="Week 1 - Big Data and Data Science submenu">
    <div class="chapter-menu">
        <div class="menu-item  graded">
            <a class="accordion-nav" href="/courses/course-v1:BerkeleyX+CS110x+2T2016/courseware/d1f293d0cb53466dbb5c0cd81f55b45b/a8482182209e4148811ac58f68a6182f/">
                <p class="accordion-display-name">Lecture 1: Big Data and Data Science </p>
                


                <p class="subtitle">
                        <span class="subtitle-name">Quizzes </span>

                            <span class="menu-icon icon fa fa-pencil-square-o" aria-hidden="true"></span>
                            <span class="sr">This content is graded</span>
                </p>
            </a>
        </div>
        <div class="menu-item  graded">
            <a class="accordion-nav" href="/courses/course-v1:BerkeleyX+CS110x+2T2016/courseware/d1f293d0cb53466dbb5c0cd81f55b45b/920d3370060540c8b21d56f05c64bdda/">
                <p class="accordion-display-name">Setting up the Course Software Environment </p>
                


                <p class="subtitle">
                        <span class="subtitle-name">Setup </span>

                            <span class="menu-icon icon fa fa-pencil-square-o" aria-hidden="true"></span>
                            <span class="sr">This content is graded</span>
                </p>
            </a>
        </div>
        <div class="menu-item active graded">
            <a class="accordion-nav" href="/courses/course-v1:BerkeleyX+CS110x+2T2016/courseware/d1f293d0cb53466dbb5c0cd81f55b45b/035f696c1172403f9253736f5dc2dccb/">
                <p class="accordion-display-name">Lab 1: Power Plant Machine Learning Pipeline <span class="sr">current section</span></p>
                


                <p class="subtitle">
                        <span class="subtitle-name">Lab due Sep 12, 2016 at 23:00 UTC</span>

                            <span class="menu-icon icon fa fa-pencil-square-o" aria-hidden="true"></span>
                            <span class="sr">This content is graded</span>
                </p>
            </a>
        </div>
        <div class="menu-item  graded">
            <a class="accordion-nav" href="/courses/course-v1:BerkeleyX+CS110x+2T2016/courseware/d1f293d0cb53466dbb5c0cd81f55b45b/735504a222bb4fd8b0cb6e37e14e8ac3/">
                <p class="accordion-display-name">Lab 1 Quiz Questions </p>
                


                <p class="subtitle">
                        <span class="subtitle-name">Quizzes </span>

                            <span class="menu-icon icon fa fa-pencil-square-o" aria-hidden="true"></span>
                            <span class="sr">This content is graded</span>
                </p>
            </a>
        </div>
    </div>
</div>

Visiting any one of those will provide us with video links, where available:

E.g. 

https://courses.edx.org/courses/course-v1:BerkeleyX+CS110x+2T2016/courseware/d1f293d0cb53466dbb5c0cd81f55b45b/a8482182209e4148811ac58f68a6182f/

<!-- one section of these PER video, seq_contents_<num> -- this blob contains HTML effectively, but
 encoded -->
  <div id="seq_contents_1"
    aria-labelledby="tab_1"
    aria-hidden="true"
    class="seq_contents tex2jax_ignore asciimath2jax_ignore">
    &lt;div class=&#34;xblock xblock-student_view xblock-student_view-vertical&#34; data-runtime-class=&#34;LmsRuntime&#34; data-init=&#34;VerticalStudentView&#34; data-course-id=&#34;course-v1:BerkeleyX+CS110x+2T2016&#34; data-request-token=&#34;bfa078c8677d11e6816012c36d13de39&#34; data-runtime-version=&#34;1&#34; data-usage-id=&#34;block-v1:BerkeleyX+CS110x+2T2016+type@vertical+block@32a94c6d23704fc389d210199ba45c53&#34; data-block-type=&#34;vertical&#34;&gt;

    &lt;div class=&#34;focus_grabber last&#34;&gt;&lt;/div&gt;
  &lt;ul class=&#34;wrapper-downloads&#34;&gt;
        &lt;li class=&#34;video-sources video-download-button&#34;&gt;
            &lt;a href=&#34;https://d2f1egay8yehza.cloudfront.net/BERCS110/BERCS1102016-V001200_DTH.mp4&#34;&gt;Download video&lt;/a&gt;
        &lt;/li&gt;


Note the download video link above.

http://stackoverflow.com/questions/2087370/decode-html-entities-in-python-string

# get content of div into a variable using beautiful soup
# call it escaped

import html
unescaped = print(html.unescape(escaped))

# Soup it again
soup = bs4.BeautifulSoup(unescaped, 'html.parser')

# Find the tags
In [32]: tags = soup.find_all('a', text=re.compile('Download video'))

Should only ever return one per blob above.

  tags = soup.find_all('div', id=lambda v: v.startswith('seq_contents_') if v else False)

  for tag in tags:
    s = parseHtml(tag.get_text()) # no unescape needed
    downloadTags = s.find_all('a', text=re.compile('Download video'))
    for downloadTag in downloadTags:
      print(downloadTag['href'])








