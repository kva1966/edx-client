import re

from bs4 import BeautifulSoup as bs

def parseHtml(data: str):
  return bs(data, 'html.parser')

with open('../docs/lecture-2.html') as f:
  data = f.read()
  soup = parseHtml(data)
  tags = soup.find_all('div', id=lambda v: v.startswith('seq_contents_') if v else False)
  for tag in tags:
    s = parseHtml(tag.get_text())
    downloadTags = s.find_all('a', text=re.compile('Download video'))
    for downloadTag in downloadTags:
      print(downloadTag['href'])

